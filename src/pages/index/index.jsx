/* eslint-disable react/no-unused-state */
import { Component } from 'react'
import Taro from '@tarojs/taro'
import { View, Input, Button, Text, Icon } from '@tarojs/components'
import './index.scss'

export default class Index extends Component {

  // 初始化值
  state = {
    hotSearch: ['北京','上海','广州','深圳','杭州','西藏','杭州','西藏'],
    searchRecord: ['南京','天津','安徽','重庆','成都'],
    inputValue: '',
  }

  // 监听输入框失去焦点时
  handleInput = (e) =>{
    let inputValue = e.detail.value.trim()
    inputValue ?
      this.setState({
        inputValue
      })
      : null
  }

  // 监听回车事件
  handleKeyDown = (e)=>{
    console.log(e)
    let inputValue = e.detail.value.trim()
    inputValue ?
      this.setState({
        inputValue
      },()=>{
        this.handleSearch()
      })
      : null
    // this.handleInput(e)
    
  }

  // 点击搜索按钮时
  handleSearch = () =>{
    const {searchRecord, inputValue} = this.state
    inputValue ?
      this.setState({
        searchRecord: [inputValue, ...searchRecord],
      })
      : null
  }

  // 点击清空按钮时
  handleClear = () =>{
    let _this = this
    Taro.showModal({
      title: '提示',
      content: '确定清空数据吗？',
      success: function (res) {
        if (res.confirm) {
          _this.setState({
            searchRecord: []
          })
        } else if (res.cancel) {
          return
        }
      }
    })
  }

  render () {
    const {hotSearch, searchRecord} = this.state
    return (
      <>
        <View className='search'>
          <View className='search-bar'>
            <Icon size='20' type='search' />
            <Input type='text' placeholder='请输入查询内容' className='search-input' onBlur={this.handleInput} onConfirm={this.handleKeyDown} />
            <Button className='search-button' onClick={this.handleSearch}>搜索</Button>
          </View>
          
        </View>
        {
          searchRecord.length > 0 ?
            <View className='search-item'>
              <View style={{display: 'flex',justifyContent: 'space-between'}}>
                <View>搜索记录</View>
                <Icon size='20' type='clear' color='#ababab' onClick={this.handleClear} />
              </View>
              <View className='search-list'>
                {
                  searchRecord.map((item,index) => <View key={index} className='list-item'>{item}</View>)
                }
              </View>
            </View>
            : null
        }
        <View className='search-item'>
          <Text>搜索热点</Text>
          <View className='search-list'>
            {
              hotSearch.map((item,index) => <View key={index} className='list-item'>{item}</View>)
            }
          </View>
        </View>
      </>
    )
  }
}
